# ycsb_m3

YCSB related crates for m3.
Tested on Linux only.

## parser
Creates a workloadfile to be used with either the `standalone_client` or the `ycsb_bench_client` in M³. A local version of the distributed [ycsb benchmark](https://github.com/brianfrankcooper/YCSB/releases/download/0.17.0/ycsb-0.17.0.tar.gz) is needed.

### Running

Assuming ycsb was unpacked in the parent directory:
```
cargo run --bin parser ../ycsb-0.17.0 workloads/workload_m3
```

### Workloads
The workload directory contains the current standart workload configuration. Use that to create bigger worloads when needed.

## standalone_client
A standalone linux client for M³'s ycsb benchmark.
Assuming a workload file `workload.wl` in this directory:
```
cargo run --bin linux_client workload.wl 127.0.0.1 1337
```

## standalone_server
Server that executes the workload of the ycsb operations send by the client. You can choose the database implementation. Either use
`kv` for an HashMap based KeyValueStore, or `sqlite` for sqlite.
```
cargo run --bin linux_server 127.0.0.1 1337 kv
```

## smoltcp client and server
Similar to the linux versions there is also a smoltcp version of the client and server. Note that the smoltcp client has one more argument. The first Ip is the source ip, the second the destination ip.


## Note
You'll have to use sudo to run the smoltcp versions, or set networking capabilities for the executables in a way, that they
can access the network interface directly.
