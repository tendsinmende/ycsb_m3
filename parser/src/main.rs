#![feature(with_options)]
use std::io::{BufRead, BufReader, BufWriter, Read, Write};
use std::path::Path;
use std::process::Command;

///Global header that describes the workload.
#[derive(PartialEq)]
pub struct WorkloadHeader {
    ///A workload is split in two phases.
    ///1. Inserts, that build the database
    ///2. Actual benchmarking operations
    ///
    ///This is the number of preparing inserts. After this the actual benchmarking operations will start. You
    ///can use this information to either benchmark the whole workload, or just the operation part of the benchmark.
    pub number_of_preinserts: u32,
    ///Number of operations including inserts and benchmarking operations.
    pub number_of_operations: u32,
}

impl WorkloadHeader {
    ///Assumes that the file writer is pointing to the correct location (usually the start of the file)
    #[allow(dead_code)]
    pub fn write_to_file<W: Write>(&self, file_buffer: &mut BufWriter<W>) {
        file_buffer
            .write(&self.number_of_preinserts.to_be_bytes())
            .unwrap();
        file_buffer
            .write(&self.number_of_operations.to_be_bytes())
            .unwrap();
    }

    #[allow(dead_code)]
    pub fn load_from_file<R: Read>(reader: &mut BufReader<R>) -> Self {
        let mut number_of_preinserts = [0 as u8; 4];
        reader.read(&mut number_of_preinserts).unwrap();
        let number_of_preinserts = u32::from_be_bytes(number_of_preinserts);

        let mut number_of_operations = [0 as u8; 4];
        reader.read(&mut number_of_operations).unwrap();
        let number_of_operations = u32::from_be_bytes(number_of_operations);
        WorkloadHeader {
            number_of_preinserts,
            number_of_operations,
        }
    }
}

///Database operations. They usually use the `key` field to determine on which record is being worked on.
///The only expection is `scan` which uses the `key` field as "start of scan" and the "scan_length" field
///to determine how many records are scanned.
#[derive(PartialEq, Debug)]
#[allow(dead_code)]
pub enum DbOp {
    Insert = 1,
    Delete = 2,
    Read = 3,
    Scan = 4,
    Update = 5,
}

///Per ycsb definition the table being worked on is always "usertable". However, since it is given as a variable in the
///original bencharm, it is included in the package as well. Should always be Usertable.
pub enum Table {
    Usertable,
}

///A single database operation.
#[derive(Debug)]
#[repr(C)]
pub struct Package {
    ///Operation on that package
    pub op: u8,
    ///Table to work on. Should always be 0
    pub table: u8,
    ///Key of the record in `table`
    pub key: u64,
    ///If an scan op, number of keys starting at `key` to scan.
    pub scan_length: u64,
    ///If `len()` is 0, this means "everything". So a Delete with `kv_pairs.len() == 0` means "delete whole record".
    ///If it has a length, the kv_pairs need to be read and worked on. The Keys are `field0`..`field9` usually. However can be more depending on the
    /// YCSB configuration. The values are long garbage strings.
    pub kv_pairs: Vec<(String, String)>,
}

impl Package {
    #[allow(dead_code)]
    fn save<W: Write>(&self, writer: &mut BufWriter<W>) {
        //write ob, table and key to file.
        let mut header = [0 as u8; 19];
        header[0] = self.op;
        header[1] = self.table;
        header[2] = self.kv_pairs.len() as u8;

        header[3..11].copy_from_slice(&self.key.to_be_bytes());
        header[11..19].copy_from_slice(&self.scan_length.to_be_bytes());
        assert!(
            writer.write(&header).unwrap() == 19,
            "Failed to write header"
        );

        //Now write out key value pairs like this
        //[num_bytes_key][num_bytes_value][key][value]
        for (k, v) in &self.kv_pairs {
            let kbytes = k.as_bytes();
            let vbytes = v.as_bytes();
            assert!(
                writer
                    .write(&[kbytes.len() as u8, vbytes.len() as u8])
                    .unwrap()
                    == 2
            );
            assert!(writer.write(&kbytes).unwrap() == kbytes.len());
            assert!(writer.write(&vbytes).unwrap() == vbytes.len());
        }
    }

    ///assumes that the reader is on the start of an header
    #[allow(dead_code)]
    pub fn load<R: Read>(reader: &mut BufReader<R>) -> Self {
        let mut header = [0 as u8; 19];
        reader.read_exact(&mut header).unwrap();

        let mut u64_buf = [0 as u8; 8];
        u64_buf.copy_from_slice(&header[3..11]);
        let key = u64::from_be_bytes(u64_buf);

        let mut u64_buf = [0 as u8; 8];
        u64_buf.copy_from_slice(&header[11..19]);
        let scan_length = u64::from_be_bytes(u64_buf);

        let num_kvs = header[2] as usize;

        let mut kv_pairs = Vec::with_capacity(num_kvs);
        //Now read all key_value_pairs
        for _ in 0..num_kvs {
            let mut length = [0 as u8; 2];
            reader.read_exact(&mut length).unwrap();

            let mut key = String::with_capacity(length[0] as usize);
            let mut value = String::with_capacity(length[1] as usize);

            reader
                .take(length[0] as u64)
                .read_to_string(&mut key)
                .unwrap();
            reader
                .take(length[1] as u64)
                .read_to_string(&mut value)
                .unwrap();
            kv_pairs.push((key, value))
        }

        Package {
            op: header[0],
            table: header[1],
            key,
            scan_length,
            kv_pairs,
        }
    }

    ///Only loads the header information of this package and returns the whole package as byte buffer. Used to read a package from a
    ///byte stream, without parsing it into the actual format.
    #[allow(dead_code)]
    pub fn load_as_bytes<R: Read>(reader: &mut BufReader<R>) -> Vec<u8> {
        //Read static sized data into bytes vec
        let mut bytes = vec![0 as u8; 19];
        reader.read_exact(&mut bytes).unwrap();

        for _i in 0..(bytes[2] as usize) {
            let mut length = [0 as u8; 2];
            reader.read_exact(&mut length).unwrap();

            let mut string_vec = vec![0 as u8; length[0] as usize + length[1] as usize];
            reader.read_exact(&mut string_vec).unwrap();

            bytes.push(length[0]);
            bytes.push(length[1]);
            bytes.append(&mut string_vec);
        }

        bytes
    }

    ///Interpretes the `data` slice as a Package. Returns Err(false) if 'data' was too short. Returns Err(true) if some
    ///information does not add up, for instance if a string can't be parsed.
    ///Returns with the (size, Package) where size is the number of bytes that have been read.
    #[allow(dead_code)]
    pub fn from_bytes(data: &[u8]) -> Result<(usize, Self), bool> {
        if data.len() < 19 {
            return Err(false);
        }
        let mut u64_buf = [0 as u8; 8];
        u64_buf.copy_from_slice(&data[3..11]);
        let key = u64::from_be_bytes(u64_buf);

        let mut u64_buf = [0 as u8; 8];
        u64_buf.copy_from_slice(&data[11..19]);
        let scan_length = u64::from_be_bytes(u64_buf);

        let num_kvs = data[2] as usize;

        let mut kv_pairs = Vec::with_capacity(num_kvs);
        //Now read all key_value_pairs
        let mut data_ptr = 19;
        for _i in 0..num_kvs {
            //println!("    {}/{}", i, num_kvs);
            if (data_ptr + 2) > data.len() {
                return Err(false);
            }
            let length = [data[data_ptr], data[data_ptr + 1]];
            //println!("    {} // {} |reads", length[0], length[1]);
            //check that the length is within the parameters
            if (data_ptr + length[0] as usize + length[1] as usize + 2) > data.len() {
                return Err(false);
            }

            data_ptr += 2;
            let key = match core::str::from_utf8(&data[data_ptr..data_ptr + length[0] as usize]) {
                Ok(st) => st,
                Err(_e) => return Err(true),
            };

            data_ptr += length[0] as usize;
            let value = match core::str::from_utf8(&data[data_ptr..data_ptr + length[1] as usize]) {
                Ok(st) => st,
                Err(_e) => return Err(true),
            };
            data_ptr += length[1] as usize;

            kv_pairs.push((key.to_string(), value.to_string()));
        }

        Ok((
            data_ptr,
            Package {
                op: data[0],
                table: data[1],
                key,
                scan_length,
                kv_pairs,
            },
        ))
    }
}

impl PartialEq for Package {
    fn eq(&self, other: &Self) -> bool {
        if self.op != other.op
            || self.table != other.table
            || self.key != other.key
            || self.scan_length != other.scan_length
        {
            return false;
        }

        for ((k, v), (ok, ov)) in self.kv_pairs.iter().zip(other.kv_pairs.iter()) {
            if k != ok || v != ov {
                return false;
            }
        }

        true
    }
}

///Opens `file_to_convert` which must contain lines of YCSB operations in ASCII format. This kind of file is generated by executing the `BasicDB` of ycsb.
///However the parser will do this automatically in its `main()`.
///
///This function will read those lines and generate a `Package` for each line. The package is then written to the `writer` and afterwards returned.
#[allow(dead_code)]
fn write_to<W: Write>(writer: &mut BufWriter<W>, file_to_convert: &str) -> Vec<Package> {
    let file = std::fs::File::open(file_to_convert).unwrap();
    let buffer = BufReader::new(file);
    let mut lines = buffer.lines();

    let mut packages: Vec<Package> = Vec::with_capacity(1024);

    //Iterate over the lines. Each line contains a single operation. If not, an warning is printed. This happens mostly when
    //the BasicDB wrote context information.
    while let Some(line) = lines.next() {
        let mut line = line.unwrap();
        //check operation type
        let tokens: Vec<_> = line.split_whitespace().map(|t| t.clone()).collect();
        let op = match tokens[0] {
            "INSERT" => DbOp::Insert,
            "DELETE" => DbOp::Delete,
            "READ" => DbOp::Read,
            "SCAN" => DbOp::Scan,
            "UPDATE" => DbOp::Update,
            _ => {
                println!("Op =>{}<= Unknown", tokens[0]);
                continue;
            }
        };

        //Make sure we are on a user table. If at some point other tables can be used, you should parse them similar to the
        //usertable here and add them to the Table enum.
        let table = match tokens[1] {
            "usertable" => Table::Usertable,
            _ => {
                panic!("Table =>{}<= unknown", tokens[1]);
            }
        };

        //For all ops other then Scan only one key follows. In case of scan, a start and end index
        //follow. In that case we use 64bit for each index and put them into the first 16 fields
        //of the key bytes.
        let mut key_str = tokens[2];
        key_str = key_str.trim_start_matches("user");
        //Parse key string into u64
        let key = key_str.parse::<u64>().unwrap();

        let scan_length = if op == DbOp::Scan {
            let scan_len = tokens[3];
            //Strip the "user" from the start token then convert to 64bit int
            scan_len.parse::<u64>().unwrap()
        } else {
            0
        };

        //At this point we have to record the fields we are inserting.

        //Find start of field table This should be the next [
        let start = line.chars().enumerate().fold(-1, |start, (idx, c)| {
            if c == '[' && start == -1 {
                idx as isize
            } else {
                start
            }
        });
        let sub_str = line.split_off(start as usize);

        //Check if we should apply to "all"
        let kv_pairs = match sub_str.as_str() {
            "[ <all fields>]" => Vec::new(),
            _ => {
                //For insert we get k,v pairs, else this are single fields
                if op == DbOp::Insert || op == DbOp::Update {
                    let mut iter = sub_str.rsplit("field");
                    let mut pairs = Vec::new();
                    while let Some(mut pair) = iter.next() {
                        //Remote leading [
                        if pair == "[ " {
                            continue;
                        }

                        if pair.ends_with(" ]") {
                            pair = pair.trim_end_matches(" ]");
                        }
                        let (k, mut value) = pair.split_at(1);
                        value = &value[1..];
                        //remove last whitespace
                        value = value.trim_end();

                        pairs.push((k.to_string(), value.to_string()));
                    }
                    pairs
                } else if op == DbOp::Delete {
                    //While ycsb seems to contain preimplementation of delete, it is never yielded as of yet (20.05.2021)
                    //therefore the parser panics if it is parsed. While you can implement it by removing this test I did not test it on the benchmark.
                    panic!("Delete not implemented yet");
                } else {
                    assert!(
                        sub_str.starts_with("[ ") && sub_str.ends_with(" ]"),
                        "Unknown fields string: {}",
                        sub_str
                    );
                    let string = &sub_str[2..(sub_str.len() - 2)];
                    vec![(string.to_string(), "NOTHING".to_string())]
                }
            }
        };

        //Save package
        let package = Package {
            op: op as u8,
            table: table as u8,
            key,
            scan_length,
            kv_pairs,
        };
        package.save(writer);
        packages.push(package);
    }
    packages
}

#[allow(dead_code)]
fn print_help() {
    println!(
        "
--- M³'s workload file generator ---
USAGE:
parser <YCSB_LOCATION> <WORKLOAD_FILE>

outputs \"workload.wl\" which can be used with either the standalone client, or be copied into
the M3 Filesystem and be used in the internal benchmark.

example: 
parser ../ycsb-0.17.0 ../ycsb-0.17.0/workload/workloada

<YCSB_LOCATION> : relative path to a distributed version of the ycsb benchmark
<WORKLOAD_FILE> : relative path to a workloadfile which will be used to generate M³'s workload file.
"
    )
}

#[allow(dead_code)]
fn main() {
    //remove old files if existing
    let _ = std::fs::remove_file("workload.wl");
    let _ = std::fs::remove_file("tmp_wl_load.txt");
    let _ = std::fs::remove_file("tmp_wl_run.txt");

    let args = std::env::args().map(|a| a.to_string()).collect::<Vec<_>>();
    if args.len() != 3 {
        print_help();
        return;
    }

    //Check that the paths exist
    if !Path::new(&args[1]).exists() || !Path::new(&args[2]).exists() {
        println!("Either the ycsb benchmark or the workload config file don't exist!");
        print_help();
        return;
    }

    //generate the ascii file continaing the load and run content
    let ycsb_path = Path::new(&args[1]).canonicalize().unwrap();
    let wlconf_path = Path::new(&args[2]).canonicalize().unwrap();

    let mut ycsb_exe_path = ycsb_path.clone();
    //add the the executable path to ycsb
    ycsb_exe_path.push("bin/ycsb.sh");
    assert!(ycsb_path.exists());

    let mut ycsb_load_path = Path::new("./").canonicalize().unwrap();
    ycsb_load_path.push("tmp_wl_load.txt");
    Command::new("touch").arg(&ycsb_load_path).output().unwrap();

    //Generate load workload
    let o = Command::new(&ycsb_exe_path)
        .arg("load")
        .arg("basic")
        .arg("-P")
        .arg(&wlconf_path)
        .output()
        .unwrap();
    //Write output to file
    std::fs::write("tmp_wl_load.txt", o.stdout).unwrap();

    //Same for the run portion
    let mut ycsb_run_path = Path::new("./").canonicalize().unwrap();
    ycsb_run_path.push("tmp_wl_run.txt");
    Command::new("touch").arg(&ycsb_run_path).output().unwrap();
    //Generate load workload
    let o = Command::new(&ycsb_exe_path)
        .arg("run")
        .arg("basic")
        .arg("-P")
        .arg(&wlconf_path)
        .output()
        .unwrap();
    std::fs::write("tmp_wl_run.txt", o.stdout).unwrap();

    //After generating the temporary ascii workload files, let the parser convert them to a single
    //binary file
    let workload_file = std::fs::File::with_options()
        .write(true)
        .append(true)
        .create_new(true)
        .open("workload.wl")
        .expect("Failed to open target file");
    let mut buffered = BufWriter::new(workload_file);

    //First write a dummy header that gets overwritten
    WorkloadHeader {
        number_of_preinserts: 0,
        number_of_operations: 0,
    }
    .write_to_file(&mut buffered);

    //Serialize insert phase
    let mut packages = write_to(&mut buffered, "tmp_wl_load.txt");
    let load_pkg_count = packages.len() as u32;

    //Serialize benchmarking phase
    packages.append(&mut write_to(&mut buffered, "tmp_wl_run.txt"));
    //make sure everything is written
    buffered.flush().unwrap();
    drop(buffered);

    //Load again and set header correctly
    let header = WorkloadHeader {
        number_of_preinserts: load_pkg_count,
        number_of_operations: packages.len() as u32,
    };
    let workload_file = std::fs::File::with_options()
        .write(true)
        .open("workload.wl")
        .expect("Failed to open target file");
    let mut buffered = BufWriter::new(workload_file);
    //Note since we did not open in append mode, this overwrites the old header information
    header.write_to_file(&mut buffered);
    buffered.flush().unwrap();
    drop(buffered);

    //Now verify the written file, by loading it the same way we would in the benchmark.
    let workload_file = std::fs::File::with_options()
        .read(true)
        .open("workload.wl")
        .expect("Failed to open target file");
    let mut reader = BufReader::new(workload_file);

    let loaded_header = WorkloadHeader::load_from_file(&mut reader);
    assert!(loaded_header == header, "Header does not match");
    println!("Testing {} operations", loaded_header.number_of_operations);
    for p in &packages {
        let loaded = Package::load(&mut reader);
        if p != &loaded {
            println!("Was not same:\np: {:?}\n loaded: {:?}", p, loaded);
        }
    }

    println!(
        "Verified all {} operations from which {} are of the load phase.",
        loaded_header.number_of_operations, loaded_header.number_of_preinserts
    );
    //Remove tmp file
    let _ = std::fs::remove_file("tmp_wl_load.txt");
    let _ = std::fs::remove_file("tmp_wl_run.txt");
}
