use std::io::Read;
use std::net::TcpListener;

use database;
use database::importer;
use database::Database;

fn print_help() {
    println!(
        "
---- M³'s standalone ycsb-bench server ----

USAGE:
standalone_server <ip> <port>

Example:
standalone_server 127.0.0.1 1337 kv

<ip>   : Ip address of this servers socket
<port> : Listening port of this servers socket
<database> : Database used can be: \"kv\" (for HashMap) | \"sqlite\" (for sqlite based database)
"
    );
}
fn main() {
    let args = std::env::args().map(|a| a.to_string()).collect::<Vec<_>>();
    if args.len() != 4 {
        print_help();
        return;
    }
    println!("Listening on {}:{}", args[1], args[2]);
    let socket = TcpListener::bind(format!("{}:{}", args[1], args[2])).unwrap();

    let mut db: Box<dyn Database> = match args[3].as_str() {
        "kv" => Box::new(database::KeyValueStore::new()),
        "sqlite" => Box::new(database::SqLite::new()),
        _ => {
            panic!("Unknown database {}", args[4])
        }
    };

    for stream in socket.incoming() {
        let mut stream = if let Ok(s) = stream {
            s
        } else {
            continue;
        };
        println!("Got connection from {:?}", stream.peer_addr());
        stream
            .set_read_timeout(Some(std::time::Duration::from_secs_f32(1.0)))
            .unwrap();
        let mut recv_buffer = vec![0 as u8; 2048];

        let server_start = std::time::Instant::now();
        let mut num_ops = 0;
        loop {
            let mut header_size_buf = [0 as u8; 4];
            let mut header_ptr = 0;
            while header_ptr < header_size_buf.len() {
                if let Ok(s) = stream.read(&mut header_size_buf[header_ptr..]) {
                    header_ptr += s;
                }
            }

            let package_size = u32::from_be_bytes(header_size_buf) as usize;
            assert!(
                package_size < recv_buffer.len(),
                "Receivbuffer was too small"
            );
            let mut package_ptr = 0;
            while package_ptr < package_size {
                if let Ok(s) = stream.read(&mut recv_buffer[package_ptr..package_size]) {
                    package_ptr += s;
                }
            }

            if package_size == 6 {
                if &recv_buffer[0..6] == b"ENDNOW" {
                    println!("Received return after {} ops!", num_ops);
                    println!(
                        "Server working time: {:.4}s",
                        server_start.elapsed().as_secs_f32()
                    );
                    db.print_stats();
                    return;
                }
            }

            match importer::Package::from_bytes(&recv_buffer[0..package_size]) {
                Ok((_read_bytes, package)) => {
                    match db.execute(package) {
                        Ok(_res) => (),
                        Err(_e) => println!("Execution failed"),
                    }
                    if (num_ops % 512) == 0 {
                        println!("Operation {} executed", num_ops);
                    }
                    num_ops += 1;
                }
                Err(should_aboard) => {
                    println!(
                        "Error, invalid package. Should aboard is : {}",
                        should_aboard
                    );
                }
            }
        }
    }
}
