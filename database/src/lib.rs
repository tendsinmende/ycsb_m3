#![feature(inline_const, with_options)]

#[path = "../../parser/src/main.rs"]
pub mod importer;

use crate::importer::{DbOp, Package};
use hashbrown::HashMap;
use std::time::{Duration, Instant};

use rusqlite::{params, Connection, Result};

struct Profiler {
    t_insert: Duration,
    t_delete: Duration,
    t_read: Duration,
    t_scan: Duration,
    t_update: Duration,
    n_insert: u64,
    n_delete: u64,
    n_read: u64,
    n_scan: u64,
    n_update: u64,
}

impl Profiler {
    pub fn new() -> Self {
        Profiler {
            t_insert: Duration::from_millis(0),
            t_delete: Duration::from_millis(0),
            t_read: Duration::from_millis(0),
            t_scan: Duration::from_millis(0),
            t_update: Duration::from_millis(0),
            n_insert: 0,
            n_delete: 0,
            n_read: 0,
            n_scan: 0,
            n_update: 0,
        }
    }

    fn print(&self) {
        //Since we have quiet different measurements, depending on the backend, change to
        //printing seconds if one time measurement is > 1sec
        if (self.t_insert.as_secs() > 0)
            || (self.t_delete.as_secs() > 0)
            || (self.t_read.as_secs() > 0)
            || (self.t_scan.as_secs() > 0)
            || (self.t_update.as_secs() > 0)
        {
            println!(
                "    Insert: {:.4}s, avg_time: {}s",
                self.t_insert.as_secs_f32(),
                self.t_insert.as_secs_f32() / self.n_insert as f32
            );
            println!(
                "    Delete: {:.4}s, avg_time: {}s",
                self.t_delete.as_secs_f32(),
                self.t_delete.as_secs_f32() / self.n_delete as f32
            );
            println!(
                "    Read:   {:.4}s, avg_time: {}s",
                self.t_read.as_secs_f32(),
                self.t_read.as_secs_f32() / self.n_read as f32
            );
            println!(
                "    Update: {:.4}s, avg_time: {}s",
                self.t_update.as_secs_f32(),
                self.t_update.as_secs_f32() / self.n_update as f32
            );
            println!(
                "    Scan:   {:.4}s, avg_time: {}s",
                self.t_scan.as_secs_f32(),
                self.t_scan.as_secs_f32() / self.n_scan as f32
            );
        } else {
            println!(
                "    Insert: {}ns, avg_time: {}ns",
                self.t_insert.subsec_nanos(),
                self.t_insert.subsec_nanos() as f32 / self.n_insert as f32
            );
            println!(
                "    Delete: {}ns, avg_time: {}ns",
                self.t_delete.subsec_nanos(),
                self.t_delete.subsec_nanos() as f32 / self.n_delete as f32
            );
            println!(
                "    Read:   {}ns, avg_time: {}ns",
                self.t_read.subsec_nanos(),
                self.t_read.subsec_nanos() as f32 / self.n_read as f32
            );
            println!(
                "    Update: {}ns, avg_time: {}ns",
                self.t_update.subsec_nanos(),
                self.t_update.subsec_nanos() as f32 / self.n_update as f32
            );
            println!(
                "    Scan:   {}ns, avg_time: {}ns",
                self.t_scan.subsec_nanos(),
                self.t_scan.subsec_nanos() as f32 / self.n_scan as f32
            );
        }
    }
}

///Implemented by any binding that is able to process the importers `Package` type.
pub trait Database {
    fn execute(&mut self, pkg: Package) -> Result<Option<Vec<u8>>, ()>;
    fn print_stats(&self);
}

pub struct KeyValueStore {
    pub tables: HashMap<u64, HashMap<String, String>>,
    profiler: Profiler,
}

impl KeyValueStore {
    pub fn new() -> Self {
        KeyValueStore {
            tables: HashMap::new(),
            profiler: Profiler::new(),
        }
    }

    fn insert(&mut self, op: Package) {
        //make sure there is such a table
        if self.tables.get(&op.key).is_none() {
            self.tables.insert(op.key, HashMap::new());
        }

        let table = self.tables.get_mut(&op.key).unwrap();
        for (k, v) in op.kv_pairs.into_iter() {
            table.insert(k, v);
        }
    }

    fn delete(&mut self, op: Package) {
        if let Some(_table) = self.tables.get_mut(&op.key) {
            println!("Delete not yet implemented");
        } else {
            println!("Server: WARNING: Tried to delete from unknown table");
        }
    }

    fn read(&mut self, op: Package) -> Option<Vec<(String, String)>> {
        if let Some(table) = self.tables.get(&op.key) {
            //If the k,v pairs are empty, this means "all fields" should be read, otherwise read only the specified ones.
            if op.kv_pairs.len() == 0 {
                Some(
                    table
                        .iter()
                        .map(|(k, v)| (k.clone(), v.clone()))
                        .collect::<Vec<_>>(),
                )
            } else {
                let mut reads = Vec::new();
                for (k, _v) in op.kv_pairs {
                    if let Some(result) = table.get(&k) {
                        reads.push((k, result.clone()));
                    }
                }
                Some(reads)
            }
        } else {
            println!("Server: WARNING: Read on unknown table");
            None
        }
    }

    fn scan(&mut self, op: Package) -> Vec<Vec<(String, String)>> {
        //In scan we walk over the records starting at op.key and going until op.key + op.scan_length.
        //For each table we add each value thats available for any key in kv_pairs, or we add every value if kv_pairs is empty.
        let mut results = Vec::new();

        //Not realy correct since the Hashmap is not in order...
        //TODO order hashmap to start at correct offset?
        let mut num_scans = 0;
        for (tk, table) in self.tables.iter() {
            if num_scans > op.scan_length {
                break;
            }

            if *tk >= op.key {
                let mut sub_results = Vec::new();

                if op.kv_pairs.len() > 0 {
                    for (k, _v) in &op.kv_pairs {
                        if let Some(val) = table.get(k) {
                            sub_results.push((k.clone(), val.clone()));
                        }
                    }
                } else {
                    //Should read all keys
                    for (k, v) in table.iter() {
                        sub_results.push((k.clone(), v.clone()))
                    }
                }

                if sub_results.len() > 0 {
                    results.push(sub_results);
                }
                num_scans += 1;
            }
        }

        results
    }

    fn update(&mut self, op: Package) {
        //println!("Update");
        if let Some(table) = self.tables.get_mut(&op.key) {
            for (k, v) in op.kv_pairs {
                if let Some(value) = table.get_mut(&k) {
                    *value = v;
                } else {
                    table.insert(k, v);
                }
            }
        } else {
            println!("Server: WARNING: Update on unknown table");
        }
    }
}

impl Database for KeyValueStore {
    ///Executes the packages action, might return a response.
    fn execute(&mut self, pkg: Package) -> Result<Option<Vec<u8>>, ()> {
        match pkg.op {
            const { DbOp::Insert as u8 } => {
                let start = Instant::now();
                self.insert(pkg);
                self.profiler.t_insert += start.elapsed();
                self.profiler.n_insert += 1;
                Ok(None)
            }
            const { DbOp::Delete as u8 } => {
                let start = Instant::now();
                self.delete(pkg);
                self.profiler.t_delete += start.elapsed();
                self.profiler.n_delete += 1;
                Ok(None)
            }
            const { DbOp::Read as u8 } => {
                let start = Instant::now();
                let res = if let Some(return_pairs) = self.read(pkg) {
                    //Convert into byte array of the form
                    //[Pair: [key_len as u8, val_len as u8][key][val]][Pair: [...]]
                    let mut return_buffer = Vec::with_capacity(return_pairs.len() * 3);
                    for (k, v) in return_pairs {
                        return_buffer.push(k.len() as u8);
                        return_buffer.push(v.len() as u8);
                        return_buffer.append(&mut k.into_bytes());
                        return_buffer.append(&mut v.into_bytes())
                    }

                    Ok(Some(return_buffer))
                } else {
                    Ok(None)
                };

                self.profiler.t_read += start.elapsed();
                self.profiler.n_read += 1;
                res
            }
            const { DbOp::Scan as u8 } => {
                let start = Instant::now();
                let _res = self.scan(pkg);
                self.profiler.t_scan += start.elapsed();
                self.profiler.n_scan += 1;
                //TODO if we ever want to send the result back we would have to assemble the return buffer here
                Ok(None)
            }
            const { DbOp::Update as u8 } => {
                let start = Instant::now();
                self.update(pkg);
                self.profiler.t_update += start.elapsed();
                self.profiler.n_update += 1;
                Ok(None)
            }
            _ => {
                println!("Unknown Op(Code: {})!", pkg.op);
                Err(())
            }
        }
    }

    fn print_stats(&self) {
        println!("Key Value Store Database Timings:");
        self.profiler.print();
    }
}

///Sqlite binding of the benchmark
pub struct SqLite {
    connection: Connection,
    profiler: Profiler,
}

impl Drop for SqLite {
    fn drop(&mut self) {
        std::fs::remove_file(Self::DB_PATH).unwrap();
    }
}

//Note implementations of Insert, Delte etc. are mostly taken from the reference implementation of ycsb.
impl SqLite {
    ///Number of fields a single entry in the database can have
    const NUM_FIELDS: usize = 10;
    const FIELD_PREFIX: &'static str = "FIELD";

    const DB_PATH: &'static str = "sqlitedb.sql";

    pub fn new() -> Self {
        {
            //In scope for dropping the created file
            std::fs::File::create(Self::DB_PATH).expect("Failed to create new db file!");
        }

        //TODO: Currently opening in memory, should be on disk
        let connection = rusqlite::Connection::open(Self::DB_PATH).unwrap();
        println!("Connected to sqlite");

        //Create the benchmarking table
        let mut execution_string = "CREATE TABLE usertable (
                  KEY              INTEGER PRIMARY KEY
                  "
        .to_string();
        //Add a field per key
        for i in 0..Self::NUM_FIELDS {
            execution_string += &format!(",{}{} VARCHAR(100)", Self::FIELD_PREFIX, i);
        }
        execution_string += ")";

        connection
            .execute(&execution_string, [])
            .expect("Failed to create benchmarking table");

        SqLite {
            connection,
            profiler: Profiler::new(),
        }
    }

    fn insert(&mut self, op: Package) {
        //For each kv pair, insert a new record if none is found for the key, or update the field.
        for (fieldname, value) in op.kv_pairs.into_iter() {
            //intln!("Insert {}:{} @ {}", fieldname, value, op.key);
            let fieldnum = fieldname.parse::<u32>().unwrap();
            assert!(
                (fieldnum as usize) < Self::NUM_FIELDS,
                "Fieldname too big: {}",
                fieldnum
            );
            let statement = format!(
                "
INSERT INTO usertable (key, {}{}) VALUES (?1, ?2) 
   ON CONFLICT(key) DO UPDATE SET {}{} = ?2
",
                Self::FIELD_PREFIX,
                fieldnum,
                Self::FIELD_PREFIX,
                fieldnum
            );
            self.connection
                .execute(&statement, params![op.key, value])
                .unwrap();
        }
    }

    fn delete(&mut self, _op: Package) {
        panic!("YCSB has no delete as of writing this. Delete unimplemented");
    }

    fn read(&mut self, op: Package) -> Option<Vec<(String, String)>> {
        //Prepare read statment
        let statement = format!("SELECT * FROM usertable where key = ?1");
        let mut stmt = self.connection.prepare(&statement).unwrap();

        if op.kv_pairs.len() == 0 {
            //Should read the whole record
            //parse and return
            //There should be only one row
            let mut res = stmt
                .query_map(params![op.key], |row| {
                    let mut v = Vec::with_capacity(Self::NUM_FIELDS);
                    for i in 0..Self::NUM_FIELDS {
                        v.push((
                            format!("{}{}", Self::FIELD_PREFIX, i),
                            row.get(1 + i).unwrap_or("EMPTY".to_string()),
                        ));
                    }
                    Ok(v)
                })
                .unwrap();

            let res_vec = res.next().unwrap().unwrap();
            Some(res_vec)
        } else {
            //Only read specified fields

            let mut res = stmt
                .query_map(params![op.key], |row| {
                    let mut v: Vec<(String, String)> = Vec::with_capacity(op.kv_pairs.len());
                    for (f, _v) in &op.kv_pairs {
                        let field_idx = f.split_at(5).1.parse::<u32>().unwrap();
                        v.push((
                            format!("{}{}", Self::FIELD_PREFIX, field_idx),
                            row.get(1 + field_idx as usize).unwrap(),
                        ));
                    }
                    Ok(v)
                })
                .unwrap();

            let res_vec = res.next().unwrap().unwrap();
            Some(res_vec)
        }
    }

    fn scan(&mut self, op: Package) -> Vec<Vec<(String, String)>> {
        let mut results = Vec::new();

        let statement = format!(
            "
SELECT * FROM usertable WHERE KEY BETWEEN ?1 AND ?2
"
        );
        let mut stmt = self.connection.prepare(&statement).unwrap();
        let res = stmt
            .query_map(params![op.key, op.scan_length], |row| {
                let mut inner_vec = Vec::with_capacity(Self::NUM_FIELDS);
                for i in 0..Self::NUM_FIELDS {
                    if let Ok(res) = row.get(1 + i) {
                        inner_vec.push((format!("{}{}", Self::NUM_FIELDS, i), res));
                    } else {
                        println!("Nothing @ {}", i);
                    }
                }

                Ok(inner_vec)
            })
            .unwrap();

        for v in res {
            let v = v.unwrap();
            if v.len() > 0 {
                results.push(v);
            }
        }

        results
    }

    fn update(&mut self, op: Package) {
        //println!("Update");
        for (k, v) in op.kv_pairs {
            let idx = k.parse::<u32>().unwrap();
            self.connection
                .execute(
                    &format!(
                        "UPDATE usertable SET {}{}=?1 where KEY = ?2",
                        Self::FIELD_PREFIX,
                        idx
                    ),
                    params![v, op.key],
                )
                .expect("Failed to update");
        }
    }
}

impl Database for SqLite {
    fn execute(&mut self, pkg: Package) -> Result<Option<Vec<u8>>, ()> {
        match pkg.op {
            const { DbOp::Insert as u8 } => {
                let start = Instant::now();
                self.insert(pkg);
                self.profiler.t_insert += start.elapsed();
                self.profiler.n_insert += 1;
                Ok(None)
            }
            const { DbOp::Delete as u8 } => {
                let start = Instant::now();
                self.delete(pkg);
                self.profiler.t_delete += start.elapsed();
                self.profiler.n_delete += 1;
                Ok(None)
            }
            const { DbOp::Read as u8 } => {
                let start = Instant::now();
                let res = if let Some(return_pairs) = self.read(pkg) {
                    //Convert into byte array of the form
                    //[Pair: [key_len as u8, val_len as u8][key][val]][Pair: [...]]
                    let mut return_buffer = Vec::with_capacity(return_pairs.len() * 3);
                    for (k, v) in return_pairs {
                        return_buffer.push(k.len() as u8);
                        return_buffer.push(v.len() as u8);
                        return_buffer.append(&mut k.into_bytes());
                        return_buffer.append(&mut v.into_bytes())
                    }

                    Ok(Some(return_buffer))
                } else {
                    Ok(None)
                };

                self.profiler.t_read += start.elapsed();
                self.profiler.n_read += 1;
                res
            }
            const { DbOp::Scan as u8 } => {
                let start = Instant::now();
                let _res = self.scan(pkg);
                self.profiler.t_scan += start.elapsed();
                self.profiler.n_scan += 1;
                //TODO if we ever want to send the result back we would have to assemble the return buffer here
                Ok(None)
            }
            const { DbOp::Update as u8 } => {
                let start = Instant::now();
                self.update(pkg);
                self.profiler.t_update += start.elapsed();
                self.profiler.n_update += 1;
                Ok(None)
            }
            _ => {
                println!("Unknown Op(Code: {})!", pkg.op);
                Err(())
            }
        }
    }

    fn print_stats(&self) {
        println!("Sqlite Database Timings:");
        self.profiler.print();
    }
}
