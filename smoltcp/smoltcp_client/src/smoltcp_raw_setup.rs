use std::collections::BTreeMap;

use smoltcp::iface::{EthernetInterfaceBuilder, NeighborCache};
use smoltcp::socket::{SocketSet, TcpSocket, TcpSocketBuffer};
use smoltcp::time::Duration;
use smoltcp::wire::{EthernetAddress, IpAddress, IpCidr};
use smoltcp::{
    iface::EthernetInterface,
    phy::RawSocket,
    socket::{SocketHandle, TcpState},
};

///Tries to parse a ipv4 from the string, assuming it looks like this "xxx.yyy.zzz.www"
pub fn ipv4_from_str(s: &str) -> Option<IpAddress> {
    let nums = s
        .rsplit('.')
        .filter_map(|part| {
            if let Ok(num) = part.parse::<u8>() {
                Some(num)
            } else {
                None
            }
        })
        .collect::<Vec<_>>();

    if nums.len() != 4 {
        println!("Could not parse {} to Ip", s);
        return None;
    }

    Some(IpAddress::v4(nums[3], nums[2], nums[1], nums[0]))
}

mod mock {
    use core::cell::Cell;
    use smoltcp::time::{Duration, Instant};

    #[derive(Debug)]
    pub struct Clock(Cell<Instant>);

    impl Clock {
        pub fn new() -> Clock {
            Clock(Cell::new(Instant::from_millis(0)))
        }

        pub fn advance(&self, duration: Duration) {
            self.0.set(self.0.get() + duration)
        }

        pub fn elapsed(&self) -> Instant {
            self.0.get()
        }
    }
}

pub struct Setup<'d> {
    clock: mock::Clock,
    iface: EthernetInterface<'d, RawSocket>,
    socket_set: SocketSet<'d>,
    socket_handle: SocketHandle,
}

impl<'d> Setup<'d> {
    ///Creates the smoltcp setup. Panics if something fails.
    pub fn new(
        self_ip: IpAddress,
        interface_name: &str,
        send_buffer_size: usize,
        recv_buffer_size: usize,
    ) -> Self {
        let clock = mock::Clock::new();
        let neighbor_cache = NeighborCache::new(BTreeMap::new());

        let ip_addrs = [IpCidr::new(self_ip, 8)];

        let device = RawSocket::new(interface_name)
            .expect("Failed to connect RawSocket to network interface");

        let iface = EthernetInterfaceBuilder::new(device)
            .ethernet_addr(EthernetAddress::default())
            .neighbor_cache(neighbor_cache)
            .ip_addrs(ip_addrs)
            .finalize();

        let socket = {
            let tcp_rx_buffer = TcpSocketBuffer::new(vec![0 as u8; recv_buffer_size]);
            let tcp_tx_buffer = TcpSocketBuffer::new(vec![0 as u8; send_buffer_size]);
            TcpSocket::new(tcp_rx_buffer, tcp_tx_buffer)
        };

        let mut socket_set = SocketSet::new(vec![Default::default()]);
        let handle = socket_set.add(socket);

        Setup {
            clock,
            iface,
            socket_set,
            socket_handle: handle,
        }
    }

    //Can be run on the socket before run is executed.
    pub fn setup(&mut self, mut function: impl FnMut(&mut TcpSocket)) {
        function(&mut self.socket_set.get::<TcpSocket>(self.socket_handle));
    }

    ///Runs self until the set update function returns true
    pub fn run(&mut self, mut function: impl FnMut(&mut TcpSocket) -> bool) {
        //When the loop should end, poll some more times to be sure that everything is send.
        let (mut got_ending_signal, mut end_iterations) = (false, 1024);

        while end_iterations > 0 {
            match self.iface.poll(&mut self.socket_set, self.clock.elapsed()) {
                Ok(_) => {}
                Err(e) => {
                    println!("poll error: {}", e);
                }
            }

            if !got_ending_signal {
                let mut socket = self.socket_set.get::<TcpSocket>(self.socket_handle);
                if socket.state() == TcpState::Established {
                    if (function)(&mut socket) {
                        got_ending_signal = true;
                    }
                } else {
                    //println!("Sender Not Connected");
                }
            } else {
                //In ending loop, reduce iteration count
                end_iterations -= 1;
            }

            match self
                .iface
                .poll_delay(&self.socket_set, self.clock.elapsed())
            {
                Some(Duration { millis: 0 }) => {}
                Some(delay) => self.clock.advance(delay),
                None => self.clock.advance(Duration::from_millis(10)),
            }
        }
    }
}
