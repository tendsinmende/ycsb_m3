#![feature(with_options)]

use std::io::BufReader;
use std::{fs::File, usize};

//include the parsers importer for loading from file
#[path = "../../../parser/src/main.rs"]
mod importer;

mod smoltcp_raw_setup;
use smoltcp::socket::TcpSocket;

fn print_help() {
    println!("
---- ycsb workload sender for M³'s key value store benchmark ----

USAGE:
standalone_client <file> <src ip> <dest ip> <dest port> <network interface>

Example:
ycsb_standalone_client workload.wl 127.0.0.1 127.0.0.2 1337 enp6s0


Note that you must either run this as superuser or set the capabilities on the executable in order to use the unix raw socket.
<file>     : Specifies the workloadfile thats being used. Must be generated via the ycsb_parser tool
<SRC IP>   : Ip that is used by this programm.
<DEST IP>  : Destination IP,
<DEST PORT>: Destination port
<INTERFACE>: The network interface to be used (the ones you see when running >ip link show<)
");
}

enum SenderState {
    LoadingPackage,
    SendingHeader {
        new_package: Option<Vec<u8>>,
        header: [u8; 4],
        ptr: usize,
    },
    SendingPackage {
        buffer: Vec<u8>,
        ptr: usize,
    },
    Finishing {
        buffer: [u8; 10],
        ptr: usize,
    },
    Quit,
}

impl SenderState {
    fn iterate(
        &mut self,
        num_ops: usize,
        num_loaded_ops: &mut usize,
        socket: &mut TcpSocket,
        reader: &mut BufReader<File>,
    ) -> bool {
        match self {
            SenderState::LoadingPackage => {
                //Check if we should stop
                if *num_loaded_ops >= num_ops {
                    let header_bytes = (6 as u32).to_be_bytes();
                    let full_buffer = [
                        header_bytes[0],
                        header_bytes[1],
                        header_bytes[2],
                        header_bytes[3],
                        b'E',
                        b'N',
                        b'D',
                        b'N',
                        b'O',
                        b'W',
                    ];
                    *self = SenderState::Finishing {
                        buffer: full_buffer,
                        ptr: 0,
                    };
                    return false;
                }

                //try to load another package, if it is failing, stop bench
                let buffer = importer::Package::load_as_bytes(reader);
                let header = (buffer.len() as u32).to_be_bytes();
                assert!(buffer.len() < 2048, "Header too long");
                *self = SenderState::SendingHeader {
                    new_package: Some(buffer),
                    header,
                    ptr: 0,
                };
                false
            }
            SenderState::SendingHeader {
                new_package,
                header,
                ptr,
            } => {
                match socket.send_slice(&header[*ptr..]) {
                    Ok(s) => *ptr += s,
                    Err(e) => println!("Failed to send header part: {}", e),
                }

                if *ptr == header.len() {
                    //move self to sending state
                    *self = SenderState::SendingPackage {
                        buffer: new_package.take().unwrap(),
                        ptr: 0,
                    };
                }
                false
            }
            SenderState::SendingPackage { buffer, ptr } => {
                match socket.send_slice(&buffer[*ptr..]) {
                    Ok(s) => *ptr += s,
                    Err(e) => println!("Failed to send package part: {}", e),
                }

                if *ptr >= buffer.len() {
                    *num_loaded_ops += 1;
                    *self = SenderState::LoadingPackage;
                }

                false
            }
            SenderState::Finishing { buffer, ptr } => {
                //Wait for the Ack
                match socket.send_slice(&buffer[*ptr..]) {
                    Ok(s) => *ptr += s,
                    Err(e) => println!("Failed to send finsh part: {}", e),
                }

                if *ptr >= buffer.len() {
                    *self = SenderState::Quit;
                }

                false
            }
            SenderState::Quit => true,
        }
    }
}

///Starts smoltcp and executes `send` once per iteratorion of the network stack
fn main() {
    //Parse arguments
    let args = std::env::args().map(|a| a.to_string()).collect::<Vec<_>>();
    if args.len() != 6 {
        println!(
            "Error: Num args does not match, should be 6, where {}",
            args.len()
        );
        print_help();
        return;
    }

    let src_addr = smoltcp_raw_setup::ipv4_from_str(&args[2]).unwrap();
    let dest_addr = smoltcp_raw_setup::ipv4_from_str(&args[3]).unwrap();
    let dest_port = args[4].parse::<u16>().expect("Could not parse det port");

    //Get file
    let workload_file = std::fs::File::open(&args[1]).expect("Could not open file");
    let mut workload = BufReader::new(workload_file);

    //Creates smoltcp setup
    let mut setup = smoltcp_raw_setup::Setup::new(src_addr, &args[5], 10 * 1024, 16 * 1024);

    setup.setup(|socket| {
        socket
            .connect((dest_addr, dest_port), (src_addr, 1337))
            .unwrap();
    });

    //Setup sending procedure
    //Now send requests until the workload file is empty
    let num_requests =
        importer::WorkloadHeader::load_from_file(&mut workload).number_of_operations as usize;
    let mut num_send_requests = 0;
    let mut state = SenderState::LoadingPackage;

    setup.run(|socket| {
        if num_requests > 0 {
            state.iterate(num_requests, &mut num_send_requests, socket, &mut workload)
        } else {
            //Should end
            let send = socket.send_slice(&(6 as u32).to_be_bytes()).unwrap();
            assert!(send == 4, "Failed to send ENDNOW header");
            let send = socket.send_slice(b"ENDNOW").unwrap();
            assert!(send == 6, "Failed to send ENDNOW");
            true
        }
    })
}
