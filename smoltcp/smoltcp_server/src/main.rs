use database::importer;
use importer::Package;

use smoltcp::socket::TcpSocket;

#[path = "../../smoltcp_client/src/smoltcp_raw_setup.rs"]
mod smoltcp_raw_setup;

use database;
use database::Database;

#[derive(PartialEq)]
enum ReceiveState {
    ReceivingHeader {
        buffer: [u8; 4],
        ptr: usize,
    },
    ReceivingPackage {
        package_size: usize,
        package_ptr: usize,
    },
    GotPackage(Option<Package>),
    Quit,
}

impl ReceiveState {
    //Might emmit a package if one was compleatly received
    fn iterate(&mut self, socket: &mut TcpSocket, receive_buffer: &mut [u8]) -> Option<Package> {
        match self {
            ReceiveState::ReceivingHeader {
                ref mut buffer,
                ref mut ptr,
            } => {
                let received = match socket.recv_slice(&mut buffer[*ptr..]) {
                    Ok(recved) => recved,
                    Err(e) => {
                        println!("Failed to receive header part: {}", e);
                        0
                    }
                };
                *ptr += received;

                //Check if we have the compleat header
                if *ptr >= buffer.len() {
                    //build package size and transform self
                    let package_size = u32::from_be_bytes(*buffer);
                    //assert!(package_size < 2048);
                    *self = ReceiveState::ReceivingPackage {
                        package_size: package_size as usize,
                        package_ptr: 0,
                    };

                    None
                } else {
                    //Continue receiving
                    None
                }
            }
            ReceiveState::ReceivingPackage {
                package_size,
                package_ptr,
            } => {
                let received =
                    match socket.recv_slice(&mut receive_buffer[*package_ptr..*package_size]) {
                        Ok(received) => received,
                        Err(e) => {
                            println!("Failed to receive package part: {}", e);
                            0
                        }
                    };

                *package_ptr += received;

                if package_ptr >= package_size {
                    //Check if the server should stop
                    if *package_size == 6 && &receive_buffer[0..6] == b"ENDNOW" {
                        *self = ReceiveState::Quit;
                        return None;
                    }
                    //Move self
                    match importer::Package::from_bytes(&receive_buffer[0..*package_size]) {
                        Ok((_psize, p)) => {
                            *self = ReceiveState::GotPackage(Some(p));
                            None
                        }
                        Err(should_abort) => {
                            println!("Package was broken, should_abort flag: {}", should_abort);
                            if should_abort {
                                *self = ReceiveState::Quit;
                                None
                            } else {
                                //Reset state
                                *self = ReceiveState::ReceivingHeader {
                                    buffer: [0; 4],
                                    ptr: 0,
                                };
                                None
                            }
                        }
                    }
                } else {
                    None
                }
            }
            ReceiveState::GotPackage(package) => {
                //Start receiving again
                let pkg = package.take().unwrap();
                *self = ReceiveState::ReceivingHeader {
                    buffer: [0; 4],
                    ptr: 0,
                };
                Some(pkg)
            }
            ReceiveState::Quit => None,
        }
    }
}

fn print_help() {
    println!(
        "
---- M³'s standalone ycsb-bench server ----

USAGE:
standalone_server <ip> <port> <interface>

Example:
standalone_server 127.0.0.1 1337 enp6s0 kv

<ip>       : Ip address of this servers socket
<port>     : Listening port of this servers socket
<interface>: Ethernet Interface to listen on.
<database> : Database used can be: \"kv\" (for HashMap) | \"sqlite\" (for sqlite based database)
"
    );
}
fn main() {
    //SimpleLogger::new().init().unwrap();
    let args = std::env::args().map(|a| a.to_string()).collect::<Vec<_>>();
    if args.len() != 5 {
        print_help();
        return;
    }

    let mut db: Box<dyn Database> = match args[4].as_str() {
        "kv" => Box::new(database::KeyValueStore::new()),
        "sqlite" => Box::new(database::SqLite::new()),
        _ => {
            panic!("Unknown database {}", args[4])
        }
    };

    let own_addr = smoltcp_raw_setup::ipv4_from_str(&args[1]).unwrap();
    let own_port = args[2].parse::<u16>().expect("Could not parse det port");

    let mut setup = smoltcp_raw_setup::Setup::new(own_addr, &args[3], 10 * 1024, 16 * 1024);

    setup.setup(|socket| {
        socket.listen(own_port).unwrap();
    });

    let mut recv_buffer = vec![0 as u8; 2048];
    let mut server_start = std::time::Instant::now();
    //Init receive state
    let mut state = ReceiveState::ReceivingHeader {
        buffer: [0; 4],
        ptr: 0,
    };
    let mut num_ops = 0;

    setup.run(|socket| {
        if let Some(new_package) = state.iterate(socket, &mut recv_buffer) {
            match db.execute(new_package) {
                Ok(_res) => (),
                Err(_e) => println!("Execution failed"),
            }
            if num_ops == 0 {
                //Reset start time to first acquired package
                server_start = std::time::Instant::now();
            }

            if (num_ops % 512) == 0 {
                println!("Operation {} executed", num_ops);
            }

            num_ops += 1;
        }

        if state == ReceiveState::Quit {
            true
        } else {
            false
        }
    });

    println!("Received return after {} ops!", num_ops);
    println!(
        "Server working time: {:.4}s",
        server_start.elapsed().as_secs_f32()
    );
    db.print_stats();
}
